package ha.org.hk.cloud.sessionws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


//@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	/*
	 * @Bean(name="testMongo") MongoTemplate mongoTemplate(MongoDbFactory
	 * mongoDbFactory, MongoConverter converter) { MongoTemplate mongoTemplate = new
	 * MongoTemplate(mongoDbFactory, converter); WriteConcern abd = new
	 * WriteConcern(6); mongoTemplate.setWriteConcern(abd);
	 * //mongoTemplate.setWriteConcernResolver(writeConcernResolver());
	 * mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
	 * System.out.println("Using Write Concern of 6"); return mongoTemplate; }
	 */
	/*
	 * @Bean public WriteConcernResolver writeConcernResolver() { return action -> {
	 * System.out.println("Using Write Concern of Acknowledged"); return
	 * WriteConcern.ACKNOWLEDGED; }; }
	 */

	/*
	 * @Bean public WebMvcConfigurer corsConfigurer() { return new
	 * WebMvcConfigurerAdapter() {
	 * 
	 * @Override public void addCorsMappings(CorsRegistry registry) {
	 * registry.addMapping("/*").allowedOrigins("*"); } }; }
	 */
}
/*
 * class MyAppWriteConcernResolver implements WriteConcernResolver {
 * 
 * @Override public WriteConcern resolve(MongoAction action) { return
 * WriteConcern.MAJORITY; }
 * 
 * }
 */
