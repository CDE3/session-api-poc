package ha.org.hk.cloud.sessionws;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

import com.mongodb.MongoClientURI;


@Configuration
@EnableMongoRepositories
public class MongoConfiguration extends AbstractMongoConfiguration {

	@Override
	protected String getDatabaseName() {
		String sessionDB = System.getenv().getOrDefault("SESSION_DB_NAME", "session");
		return sessionDB;
		
	}

	@Override
	protected String getMappingBasePackage() {
		return "ha.org.hk.cloud.sessionws.DBO";
	}


	@Override
	public MongoClient mongoClient() {
		String sessionDB = System.getenv().getOrDefault("SESSION_DB_URI", "mongodb://160.200.130.144:27017,160.200.130.143:27017,160.200.130.142:27017,160.88.114.233:27017,160.88.114.227:27017/springo?replicaSet=hapoc&retryWrites=true&readConcernLevel=majority&w=majority");
		//"mongodb://160.200.130.144:27017,160.200.130.143:27017,160.200.130.142:27017,160.88.114.233:27017,160.88.114.227:27017/springo?replicaSet=hapoc&retryWrites=true&readConcernLevel=majority&w=majority"
		// MongoClientOptions opt = new MongoClientOptions.builder().retryWrites(true);

		// MongoClientOptions.builder().
		// MongoClient client = new MongoClient(new
		// MongoClientURI("mongodb://160.81.99.121:27017,160.81.99.122:27017,160.81.99.123:27017,160.88.114.233:27017,160.88.114.227:27017/springo?replicaSet=hapoc&retryWrites=true&readConcernLevel=majority&w=majority"));
		// MongoClient client = new MongoClient(new
		// MongoClientURI("mongodb://160.81.99.121:27017,160.81.99.122:27017,160.81.99.123:27017,160.88.114.233:27017,160.88.114.227:27017/springo?replicaSet=hapoc&retryWrites=true&w=majority"));
		// MongoClient client = new MongoClient(new
		// MongoClientURI("mongodb://user7X4:fdTVl6uvuRN2idfL@mongodb-poc2:27017/session"));
		MongoClient client = new MongoClient(new MongoClientURI(sessionDB));
		/*
		 * ServerAddress serverAddress = new ServerAddress("mongodb-poc2",27017);
		 * List<ServerAddress> addrs = new ArrayList<ServerAddress>();
		 * addrs.add(serverAddress);
		 * 
		 * 
		 * MongoCredential credential =
		 * MongoCredential.createScramSha1Credential("user7X4", "session",
		 * "fdTVl6uvuRN2idfL".toCharArray()); List<MongoCredential> credentials = new
		 * ArrayList<MongoCredential>(); credentials.add(credential);
		 * 
		 * MongoClient client = new
		 * MongoClient(addrs,credentials,MongoClientOptions.builder().
		 * minConnectionsPerHost(100).build());
		 */

		return client;

	}
}