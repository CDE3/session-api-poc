package ha.org.hk.cloud.sessionws.DBO;

import java.util.Date;

import javax.annotation.Resource;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

@Component
public class SessionDaoImpl implements SessionDao {

	@Resource
	private MongoTemplate mongoTemplate;
	private String collectionName = "session";
	
	@Override
	public boolean saveSession(String key, String func, String JSON, boolean deleteFlag) {
		BasicDBObject query1 = new BasicDBObject("session_key", key);
		MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
		FindIterable<Document> result = collection.find(query1);
		
		if (!result.iterator().hasNext()) {
			Document dbObject = new Document("session_key", key);

			dbObject.append(func, com.mongodb.BasicDBObject.parse(JSON));
			Date d1 = new Date();
			dbObject.append("created_dt", d1);
			dbObject.append("last_updated", d1);
			Document ud_result = mongoTemplate.save(dbObject, collectionName);		
			
		} else {
			Query query = new Query(Criteria.where("session_key").is(key));
			Update update = new Update();
			if (deleteFlag) {
				update.unset(func);
			} else {
				update.set(func, com.mongodb.BasicDBObject.parse(JSON));
			}
			update.set("last_updated", new Date());
			UpdateResult ud_result = mongoTemplate.updateFirst(query, update, collectionName);
		}
		return true;
	}


	@Override
	public int refreshSession(String key) {
		//BasicDBObject query1 = new BasicDBObject("session_key", key);
		/*MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
		FindIterable<Document> result = collection.find(query1);
		if (!result.iterator().hasNext()) {			
			return 0;
		} else {*/
			Query query = new Query(Criteria.where("session_key").is(key));
			Update update = new Update();			
			update.set("last_updated", new Date());
			UpdateResult ud_result = mongoTemplate.updateFirst(query, update, collectionName);
			return (int)ud_result.getModifiedCount();
		//}
	}

	@Override
	public int updateSession(String key, String func, String JSON, boolean deleteFlag) {
		BasicDBObject query1 = new BasicDBObject("session_key", key);
		MongoCollection<Document> collection2 = mongoTemplate.getCollection(collectionName);
		FindIterable<Document> result = collection2.find(query1);

		if (!result.iterator().hasNext()) {
			return 0;
		} else {
			try {
			//	JSONObject existingObj = null;
				//System.out.println(result.first().toJson());
				//existingObj = new JSONObject(result.first().toJson());

				Query query = new Query(Criteria.where("session_key").is(key));

				Update update = new Update();
				if (deleteFlag) {
					// delete part of the function
					update.unset(func);
				} else {

				/*	if (existingObj.has(func)) {
						JSONObject funcObj = (JSONObject) existingObj.get(func);
						JSONObject updateObj = new JSONObject(JSON);
						JSONObject finalObj = SessionUtil.deepMerge(funcObj, updateObj);
						update.set(func, com.mongodb.BasicDBObject.parse(finalObj.toString()));
					} else {*/
						update.set(func, com.mongodb.BasicDBObject.parse(JSON));
					//}

				}

				update.set("last_updated", new Date());

				UpdateResult ud_result = mongoTemplate.updateFirst(query, update, collectionName);
				return (int)ud_result.getModifiedCount();
			} catch (JSONException e) {
				
				e.printStackTrace();
				return 0;
			}
			
		}

	}

	@Override
	public int removeSession(String key) {		
		MongoCollection<Document> collection = mongoTemplate.getCollection(collectionName);
		DeleteResult result = collection.deleteOne(Filters.eq("session_key", key));
		return (int)result.getDeletedCount();
	}

	@Override
	public String findSessionByKey(String key) {

		// sessionEntity = mongoTemplate.findOne(query, SessionEntity.class);
		int exception = 0;
		int MAX_RETRY = 10;
		int RETRY_TIME = 1000;
		do {
			try {
				// System.out.println("retrying "+exception);
				BasicDBObject query1 = new BasicDBObject("session_key", key);

				MongoCollection<Document> collection2 = mongoTemplate.getCollection(collectionName);
				FindIterable<Document> result = collection2.find(query1);
				if (result != null && result.first() != null) {
					return result.first().toJson();
				}
				exception = 0;
			} catch (Exception e) {
				exception = exception + 1;
				//System.out.println("retry " + exception);
				try {
					Thread.sleep(RETRY_TIME);
				} catch (InterruptedException e1) {
					
					e1.printStackTrace();
				}
			}
		} while (exception > 0 && exception <= MAX_RETRY);
		
		return null;

	}

	@Override
	public String findSessionByKey(String key, String func) {

		// sessionEntity = mongoTemplate.findOne(query, SessionEntity.class);
		int exception = 0;
		int MAX_RETRY = 10;
		int RETRY_TIME = 1000;
		do {
			try {
				// System.out.println("retrying "+exception);
				BasicDBObject query1 = new BasicDBObject("session_key", key);

				MongoCollection<Document> collection2 = mongoTemplate.getCollection(collectionName);
				FindIterable<Document> result = null;
				if (func.equalsIgnoreCase("common") || func.equalsIgnoreCase("psp")) {
					result = collection2.find(query1).projection(Projections.include("common", "psp", "session_key"));

				} else {
					result = collection2.find(query1)
							.projection(Projections.include("common", "psp", "session_key", func));
				}

				if (result != null && result.first() != null) {
					//System.out.println(result.first().toJson());
					return result.first().toJson();
				}
				exception = 0;
			} catch (Exception e) {
				exception = exception + 1;
				//System.out.println("retry " + exception);
				try {
					Thread.sleep(RETRY_TIME);
				} catch (InterruptedException e1) {
					
					e1.printStackTrace();
				}
			}
		} while (exception > 0 && exception <= MAX_RETRY);
		return null;

	}

}
