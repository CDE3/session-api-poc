package ha.org.hk.cloud.sessionws.DBO;

public interface  SessionDao  {

    public int removeSession(String key);

    public String findSessionByKey(String key);

    public boolean saveSession(String key, String func, String JSON,boolean deleteFlag);

    public int refreshSession(String key);

    public String findSessionByKey(String key, String func);

	public int updateSession(String key, String func, String JSON, boolean deleteFlag);
}

