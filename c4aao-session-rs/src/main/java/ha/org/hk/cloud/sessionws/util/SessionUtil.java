package ha.org.hk.cloud.sessionws.util;


import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.json.*;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SessionUtil {
	public static final String encryption_key = System.getenv().getOrDefault("SESSION_ACCESS_ENCRYPTION_KEY",
			"ENCRYPTION_key_1");
	public static final String encryption_ver = System.getenv().getOrDefault("SESSION_ACCESS_ENCRYPTION_VER", "V1,V2");
	public static final String encryption_hdr = System.getenv().getOrDefault("SESSION_ACCESS_ENCRYPTION_HDR",
			"SESSION_ACCESS_HDR");
	public static final SecretKey encryption_key_ase = new SecretKeySpec(encryption_key.getBytes(), "AES");
	public static final String access_code_separateor = "#";

	public static JSONObject mergeJSONObjects(JSONObject inputJson1, JSONObject inputJson2) {
		JSONObject mergedObj = new JSONObject();

		Iterator<String> i1 = inputJson1.keys();
		Iterator<String> i2 = inputJson2.keys();
		String tmp_key;
		while (i1.hasNext()) {
			tmp_key = (String) i1.next();
			try {
				mergedObj.put(tmp_key, inputJson1.get(tmp_key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while (i2.hasNext()) {
			tmp_key = (String) i2.next();
			try {
				mergedObj.put(tmp_key, inputJson2.get(tmp_key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mergedObj;
	}

	public static JSONObject deepMerge(JSONObject orginal, JSONObject update) throws JSONException {
		Iterator<String> i1 = update.keys();
		String key;
		while (i1.hasNext()) {
			key = (String) i1.next();
			Object value = update.get(key);
			if (!orginal.has(key)) {
				// new value for "key":
				orginal.put(key, value);
			} else {
				// existing value for "key" - recursively deep merge:
				if (value instanceof JSONObject && orginal.get(key) instanceof JSONObject) {
					JSONObject valueJson = (JSONObject) value;
					deepMerge(orginal.getJSONObject(key), valueJson);
				} else {
					orginal.put(key, value);
				}
			}
		}

		return orginal;
	}

	public static String generateJson(HashMap<String, String> input) {
		String result2 = "";
		Iterator<Map.Entry<String, String>> it = input.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pair = it.next();

			// System.out.println(pair.getKey() + " = " + pair.getValue());
			if ("".equalsIgnoreCase(result2)) {
				result2 += "\"" + pair.getKey() + "\":" + pair.getValue();
			} else {
				result2 += ",\"" + pair.getKey() + "\":" + pair.getValue();
			}

			it.remove(); // avoids a ConcurrentModificationException
		}
		if ("".equalsIgnoreCase(result2)) {
			result2 = "{}";
		} else {
			result2 = "{" + result2 + "}";
		}
		return result2;
	}

	public static boolean isJSONValid(String test) {
		try {
			new JSONObject(test);
		} catch (JSONException ex) {

			try {
				new JSONArray(test);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}

	public static enum errorType {
		JSON, ACCESSCODE, KEYNOTEXISTS
	}

	public static ResponseEntity<String> errorHttpReturn(String type) {
		errorType x = errorType.valueOf(type);
		switch (x) {
		case JSON:
			return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).body(
					"{" + "\"ErrorCode\": \"INVALID_JSON_BODY\"," + "\"Description\": \"INVALID JSON BODY\"" + "}");
		case ACCESSCODE:
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
					"{" + "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID ACCESS CODE\"" + "}");
		case KEYNOTEXISTS:
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.body("{" + "\"ErrorCode\": \"KEY_NOT_EXISTS\"," + "\"Description\": \"KEY NOT EXISTS\"" + "}");
		default:
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{" + "\"ErrorCode\": \"INVALID_REQUEST\"," + "\"Description\": \"INVALID REUEST\"" + "}");
		}
	}

	public static void writeLog(String msg) {
		System.out.println(getTime() + " " + msg);

	}

	public static String getTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}

	public static String encrypt(String str) throws Exception {
		// Encode the string into bytes using utf-8
		byte[] utf8 = str.getBytes("UTF8");
		Cipher ecipher;
		ecipher = Cipher.getInstance("AES");
		ecipher.init(Cipher.ENCRYPT_MODE, encryption_key_ase);

		// Encrypt
		byte[] enc = ecipher.doFinal(utf8);
		String s1 = Base64.getEncoder().encodeToString(enc);
		System.out.println(s1);
		String s = Base64.getUrlEncoder().encodeToString(enc);
		// Encode bytes to base64 to get a string
		return s;
	}

	public static String decrypt(String str) throws Exception {
		// Decode base64 to get bytes
		byte[] dec = Base64.getUrlDecoder().decode(str);
		Cipher dcipher;
		dcipher = Cipher.getInstance("AES");
		dcipher.init(Cipher.DECRYPT_MODE, encryption_key_ase);

		byte[] utf8 = dcipher.doFinal(dec);

		// Decode using utf-8
		return new String(utf8, "UTF8");
	}

	public static boolean authenicateAction(String func, String access_code, String action) throws Exception {
		try {
			System.out.println("Auth:" + access_code);
			// access_code = access_code.replace("$", "//");

			String decrypted = decrypt(access_code);
			System.out.println("decrypted:" + decrypted);
			String[] result = decrypted.split(access_code_separateor);
			String strHeader = result[0];
			String strVer = result[1];
			String strFuncID = result[2];
			String strAccessRight = result[3];
			/*
			 * System.out.println(strHeader+","+strVer+","+strFuncID+","+strAccessRight);
			 * System.out.println(encryption_hdr+","+encryption_ver+","+func+","+action);
			 * System.out.println(strHeader.equalsIgnoreCase(encryption_hdr));
			 * System.out.println(encryption_ver.contains(strVer));
			 * System.out.println((strFuncID.equals(func)));
			 * System.out.println(strAccessRight.contains(action));
			 */
			if (strHeader.equalsIgnoreCase(encryption_hdr) && encryption_ver.contains(strVer) && strFuncID.equals(func)
					&& strAccessRight.contains(action)) {
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;

		}
		return false;

	}

	public static String genAccessCode(String func, String version, String strAccessRight) throws Exception {
		String encrypted = "";
		try {
			String content = encryption_hdr + access_code_separateor + version + access_code_separateor + func
					+ access_code_separateor + strAccessRight;
			encrypted = encrypt(content);
			System.out.println(encrypted);
			//encrypted = URLEncoder.encode(encrypted, "UTF-8");
			System.out.println(encrypted);
		} catch (Exception e) {
			return "";
		}
		return encrypted;

	}

	public static String generateUniqueKey() {
        String key = null;

        try {
            MessageDigest salt = MessageDigest.getInstance("SHA-256");
            salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
            key = bytesToHex(salt.digest());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return key;
    }

    private static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (final byte b: in)
            builder.append(String.format("%02x", b));
        return builder.toString();
    }

}
