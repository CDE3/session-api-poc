package ha.org.hk.cloud.sessionws;

import java.util.HashMap;

import java.util.Optional;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ha.org.hk.cloud.sessionws.DBO.SessionDao;
import ha.org.hk.cloud.sessionws.util.SessionUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SessionController {

	@Autowired
	private SessionDao sessionDao;
/*
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping(value = "/session/v1/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> POST(HttpServletRequest request,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password) throws Exception {

		HashMap<String, String> result = new HashMap<String, String>();
		if (password.equalsIgnoreCase("abc123")) {
			result.put("login_status", "\"Y\"");
			UUID uuid = UUID.randomUUID();
			result.put("c4aao-session-key", "\"" + uuid.toString() + "\"");
			POST(null, "{\"username\":\"" + username + "\"}", uuid.toString(), "common", null);
		} else {
			result.put("login_status", "\"N\"");
		}

		return ResponseEntity.ok(SessionUtil.generateJson(result));
	}
*/
	/*
	 * @CrossOrigin(origins = "*", allowedHeaders = "*")
	 * 
	 * @PostMapping(value = "/session/v1/POST/{key}/{func}", consumes =
	 * MediaType.APPLICATION_JSON_VALUE, produces =
	 * MediaType.APPLICATION_JSON_VALUE) public ResponseEntity<?>
	 * POST(HttpServletRequest request, @RequestBody(required = false) String qBody,
	 * 
	 * @PathVariable String key, @PathVariable String func ) throws Exception {
	 * return POST_AUTH(request,qBody,key,func,""); }
	 */
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping(value =  "/session/v1/POST/{func}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> POST(HttpServletRequest request, @RequestBody(required = false) String qBody,
			 @PathVariable String func, @RequestHeader("x-api-key") Optional<String> access_code)
			throws Exception {
		//System.out.println(access_code);
		if (access_code.isPresent() && !SessionUtil.authenicateAction(func, access_code.get(), request.getMethod())) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID
			// ACCESS CODE\"" + "}");
			return SessionUtil.errorHttpReturn("ACCESSCODE");
		}
		if (SessionUtil.isJSONValid(qBody) == false) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_JSON_BODY\"," + "\"Description\": \"INVALID JSON
			// BODY\"" + "}");
			return SessionUtil.errorHttpReturn("JSON");
		}
		String sessionEntity = "";
		String sessionKey = "";
		boolean createSessionDone = false;
		int retry_cnt = 0;
		do {
			
			String session_key = SessionUtil.generateUniqueKey();
			String checkKey = sessionDao.findSessionByKey(session_key);
			if (checkKey == null) {
				sessionDao.saveSession(session_key, func, qBody, false);
				sessionKey = session_key;
				createSessionDone = true;
			}
			retry_cnt++;

		} while (!createSessionDone && retry_cnt < 10);

		/*
		 * if (sessionEntity != null) { sessionDao.saveSession(session_key, func, qBody,
		 * false);
		 * 
		 * SessionUtil.writeLog(" Session Post exists (Key) (" + session_key + ") ");
		 * 
		 * } else { try { sessionDao.saveSession(key, func, qBody, false);
		 * 
		 * } catch (Exception e) { if (e.getMessage().contains("duplicate key")) {
		 * SessionUtil.writeLog("session post duplicated(" + key + ") "); } else {
		 * return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
		 * "\"ErrorCode\": \"Exception\"," + "\"Description\": \"" + e.getMessage() +
		 * "\"" + "}"); }
		 * 
		 * }
		 * 
		 * SessionUtil.writeLog("session post (" + key + ") "); }
		 */

		sessionEntity = sessionDao.findSessionByKey(sessionKey, func);

		return ResponseEntity.ok(sessionEntity);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PutMapping(value = 
			"/session/v1/PUT/{key}/{func}" , consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> PUT(HttpServletRequest request, @RequestBody(required = false) String qBody,
			@PathVariable String key, @PathVariable String func, @RequestHeader("x-api-key") Optional<String> access_code)
			throws Exception {
				
		if (access_code.isPresent() && !SessionUtil.authenicateAction(func, access_code.get(),request.getMethod())) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID
			// ACCESS CODE\"" + "}");
			return SessionUtil.errorHttpReturn("ACCESSCODE");
		}
		if (SessionUtil.isJSONValid(qBody) == false) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_JSON_BODY\"," + "\"Description\": \"INVALID JSON
			// BODY\"" + "}");
			return SessionUtil.errorHttpReturn("JSON");
		}

		String sessionEntity = sessionDao.findSessionByKey(key);

		if (sessionEntity == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{" + "\"ErrorCode\": \"KEY_NOT_EXISTS\"," + "\"Description\": \"KEY NOT EXISTS\"" + "}");
		} else {
			sessionDao.saveSession(key, func, qBody, false);

		}
		sessionEntity = sessionDao.findSessionByKey(key, func);

		SessionUtil.writeLog("session put (" + key + ") ");

		return ResponseEntity.ok(sessionEntity);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/session/v1/GET/{key}/{func}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(HttpServletRequest request, @RequestHeader("x-api-key") Optional<String> access_code , @RequestBody(required = false) String pBody,
			@PathVariable String key, @PathVariable String func)
			throws Exception {
				
			if (access_code.isPresent() && !SessionUtil.authenicateAction(func, access_code.get(), request.getMethod())) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID
			// ACCESS CODE\"" + "}");
			return SessionUtil.errorHttpReturn("ACCESSCODE");
		}
		String sessionEntity = sessionDao.findSessionByKey(key, func);
		if (sessionEntity == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{" + "\"ErrorCode\": \"KEY_NOT_EXISTS\"," + "\"Description\": \"KEY NOT EXISTS\"" + "}");
		}
		SessionUtil.writeLog("session get (" + key + ")");

		return ResponseEntity.ok(sessionEntity);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/session/v1/REFRESH/{key}/{func}" , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> refresh(HttpServletRequest request, @RequestBody(required = false) String pBody,
			@PathVariable String key, @PathVariable String func, @RequestHeader("x-api-key") Optional<String> access_code)
			throws Exception {
		if (access_code.isPresent() && !SessionUtil.authenicateAction(func, access_code.get(), "REFRESH")) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID
			// ACCESS CODE\"" + "}");
			return SessionUtil.errorHttpReturn("ACCESSCODE");
		}
		//String sessionEntity = sessionDao.findSessionByKey(key);
		//if (sessionEntity == null) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"KEY_NOT_EXISTS\"," + "\"Description\": \"KEY NOT EXISTS\""
			// + "}");
		//	return SessionUtil.errorHttpReturn("KEYNOTEXISTS");
	//	}
		SessionUtil.writeLog("session refresh (" + key + ")");
		int resultCnt = sessionDao.refreshSession(key);

		HashMap<String, String> result = new HashMap<String, String>();
		result.put("key", "\"" + key + "\"");
		result.put("func", "\"" + func + "\"");
		if(resultCnt >0){
			result.put("refresh_result", "\"Y\"");
		}else{
			return SessionUtil.errorHttpReturn("KEYNOTEXISTS");
			//result.put("refresh_result", "\"Y\"");
		}
		
		return ResponseEntity.ok(SessionUtil.generateJson(result));
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value =  "/session/v1/DELETE/{key}/{func}" , method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(HttpServletRequest request, @RequestBody(required = false) String pBody,
			@PathVariable String key, @PathVariable String func,@RequestHeader("x-api-key") Optional<String> access_code)
			throws Exception {
		if (access_code.isPresent() && !SessionUtil.authenicateAction(func, access_code.get(), request.getMethod())) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"INVALID_ACCESS_CODE\"," + "\"Description\": \"INVALID
			// ACCESS CODE\"" + "}");
			return SessionUtil.errorHttpReturn("ACCESSCODE");
		}
		String sessionEntity = sessionDao.findSessionByKey(key);

		if (sessionEntity == null) {
			// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{" +
			// "\"ErrorCode\": \"KEY_NOT_EXISTS\"," + "\"Description\": \"KEY NOT EXISTS\""
			// + "}");
			return SessionUtil.errorHttpReturn("JSON");
		}

		if (!func.equalsIgnoreCase("common")) {
			sessionDao.saveSession(key, func, "{}", true);
		} else {
			sessionDao.removeSession(key);
		}
		SessionUtil.writeLog("session delete (" + key + ")");
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("key", "\"" + key + "\"");
		result.put("func", "\"" + func + "\"");
		result.put("delete_result", "\"Y\"");
		return ResponseEntity.ok(SessionUtil.generateJson(result));

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping(value = "/session/v1/GENACCESSCODE/{func}/{ver}/{access_grant}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> GenAccessCode(HttpServletRequest request, @RequestBody(required = false) String qBody,
			@PathVariable String ver, @PathVariable String func, @PathVariable String access_grant) throws Exception {
		String resultAC = SessionUtil.genAccessCode(func, ver, access_grant);
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("access_code", "\"" + resultAC + "\"");
		result.put("func", "\"" + func + "\"");
		return ResponseEntity.ok(SessionUtil.generateJson(result));
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/healthCheck.jsp")
	public ResponseEntity<?> healthCheck() {

		return ResponseEntity.ok("healthy");

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/healthCheck")
	public ResponseEntity<?> healthCheck2() {

		return ResponseEntity.ok("healthy");

	}

}
